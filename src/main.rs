// Crates
extern crate gitlab;
#[macro_use] extern crate serde_derive;
extern crate toml;
extern crate urlencoding;

extern crate json;
extern crate reqwest;

// Modules
mod state;
mod error;

// Standard
use std::{thread, time};

// Library
use gitlab::{
	Gitlab,
	MergeRequestStateFilter,
	types::{UserPublic, ProjectId, Project, MergeRequest},
};
use reqwest::Client;
use json::JsonValue;

// Local
use error::Error;
use state::State;

fn build_uri(token: &str, path: &str, attr: &str) -> String {
	format!("https://www.gitlab.com/api/v4{}?private_token={}&{}", path, token, attr)
}

fn create_issue(state: &State, client: &Client, proj_id: u64, title: &str, labels: &str, desc: &str) -> Result<u64, Error> {
	let body = format!("{{ \"id\": {}, \"title\": {} }}", proj_id, urlencoding::encode(title));

	let resp_str = client
		.post(&build_uri(
            &state.token,
            &format!("/projects/{}/issues", proj_id),
            &format!("title={}&labels={}&description={}", urlencoding::encode(title), labels, urlencoding::encode(desc))
        ))
		.body(body)
		.send()?
        .text()?;

    if let JsonValue::Object(obj) = json::parse(&resp_str)? {
        match obj.get("iid") {
            Some(JsonValue::Number(iid)) => { return Ok(iid.as_parts().1); },
            _ => Err(Error::from("No 'iid' value in JSON")),
        }
    } else {
        return Err(Error::from("JSON parsing failed"));
    }
}

fn check_mr_title(title: &str) -> Option<(String, String)> {
	let parts = title.split(',').map(|s| s.trim()).collect::<Vec<&str>>();

	if parts.len() >= 3 {
		if parts[0] == "NEW" {
			return Some((parts[1].to_string(), parts[2].to_string()))
		}
	}
	None
}

fn create_rfc(state: &mut State, gl: &Gitlab, user: &UserPublic, proj: &Project, mr: &MergeRequest, rfc_name: String, rfc_title: String) {
    let rfc_name = rfc_name.replace(' ', "_").replace('-', "_").to_lowercase();
    if let Ok(notes) = gl.merge_request_notes(proj.id, mr.iid) {
        if !notes.into_iter().any(|n| {
            n.body.contains("beep boop") &&
            n.author.username == user.username
        }) {
            let rfc_num = state.generate_rfc_number();

            let rfc_rendered_url = format!("{0}/open/{1:04}-{2}.md", proj.web_url, rfc_num, rfc_name);

            println!("[INFO] Creating Reqwest client...");
            let client = Client::new();
            let rfc_issue = create_issue(
                &state,
                &client,
                proj.id.value(),
                &format!("Tracking {:04} ({}): {}", rfc_num, rfc_name, rfc_title),
                "open",
                &format!("# {}\n\nShorthand name: *{}*\n\n## [Rendered RFC]({})", rfc_title, rfc_name, rfc_rendered_url),
            ).expect("Could not create issue");

            gl.set_merge_request_labels(proj.id, mr.iid, ["new-rfc"].iter())
                .expect("Failed to leave note on merge request");

            gl.create_merge_request_note(
                proj.id,
                mr.iid,
                &format!(
                    "*Thank you for submitting an RFC, {3}.* \
                    \n\n**The number for this RFC is `{0:04}`. The RFC issue is #{1}.** \
                    \n\nThere are a few things you need to do before it is merged and submitted: \
                    \n\n1) Change the name of your RFC file to `{0:04}-{2}.md`
                    \n\n2) Change the header data in `{0:04}-{2}.md` to reflect the information above \
                    \n\n3) Ensure `{0:04}-{2}.md` is within the `open/` directory \
                    \n\n4) Commit your changes so they appear within this merge request
                    \n\n---\n\n \
                    **beep boop**, I'm a bot. Please email *joshua.s.barretto@gmail.com* if I got something wrong!",
                    rfc_num,
                    rfc_issue,
                    rfc_name,
                    mr.author.name,
                )
            ).expect("Failed to leave note on merge request");
            println!("[INFO] Allocated RFC number for merge request {}", mr.id);
        }
    } else {
        println!("[WARN] Could not access notes for project '{}'", proj.name);
    }
}

fn main() {
    println!("[INFO] Loading state from file...");
    let mut state = State::load_from("config.toml")
        .expect("Could not load state");
    println!("[INFO] Loaded state");

	println!("[INFO] Connecting to GitLab...");
	let gl = loop {
		match Gitlab::new("www.gitlab.com", &state.token) {
			Ok(gl) => break gl,
			Err(e) => {
				println!("[WARNING] Could not connect to GitLab: {:?}", e);
				println!("[INFO] Retrying...");
			},
		}
	};
	println!("[INFO] Connected");

	println!("[INFO] Identifying user...");
	let user = gl.current_user().expect("Could not identify user");
	println!("[INFO] Starting as user '{}' ({})", user.name, user.username);

	println!("[INFO] Accessing project with ID '{}'...", state.project_id);
	let proj = gl.project(ProjectId::new(state.project_id))
		.expect("Could not access project");
	println!("[INFO] [{}] Accessed project", proj.name);

    loop {
		for mr in gl.merge_requests_with_state(proj.id, MergeRequestStateFilter::Opened)
			.expect("Could not access merge requests")
		{
			println!("[INFO] [{}] Found open merge request {} with title '{}'", proj.name, mr.id, mr.title);
			if let Some((rfc_name, rfc_title)) = check_mr_title(&mr.title) {
                create_rfc(&mut state, &gl, &user, &proj, &mr, rfc_name, rfc_title);
			}
		}

		break;
		//println!("[INFO] All tasks finished, sleeping for 30 seconds");
		//thread::sleep(time::Duration::from_secs(30));
	}

	println!("[INFO] Saving state to file...");
    state.save_to("config.toml")
        .expect("Failed to save state to file");
    println!("[INFO] Saved state");
}
