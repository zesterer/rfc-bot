// Standard
use std::io;

// Library
use reqwest;
use json;
use toml::{ser, de};

#[derive(Debug)]
pub enum Error {
	StringErr(String),
}

impl From<reqwest::Error> for Error {
	fn from(err: reqwest::Error) -> Error { Error::StringErr(format!("{:?}", err)) }
}

impl From<json::Error> for Error {
	fn from(err: json::Error) -> Error { Error::StringErr(format!("{:?}", err)) }
}

impl From<io::Error> for Error {
	fn from(err: io::Error) -> Error { Error::StringErr(format!("{:?}", err)) }
}

impl From<de::Error> for Error {
	fn from(err: de::Error) -> Error { Error::StringErr(format!("{:?}", err)) }
}

impl From<ser::Error> for Error {
	fn from(err: ser::Error) -> Error { Error::StringErr(format!("{:?}", err)) }
}

impl<'a> From<&'a str> for Error {
	fn from(err: &'a str) -> Error { Error::StringErr(err.to_string()) }
}
