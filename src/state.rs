// Standard
use std::fs::File;
use std::io::prelude::*;

// Library
use toml;

// Local
use Error;

#[derive(Deserialize, Serialize)]
pub struct State {
	pub token: String,
    pub project_id: u64,
	rfc_counter: u64,
}

impl State {
    pub fn load_from(path: &str) -> Result<State, Error> {
        let mut file = File::open(path)?;
        let mut toml_str = String::new();
        file.read_to_string(&mut toml_str).expect("Could not read config file");

        Ok(toml::from_str(&toml_str)?)
    }

    pub fn save_to(self, path: &str) -> Result<(), Error> {
        let toml_str = toml::to_string(&self)?;

        let mut file = File::create(path)?;
        file.write_all(toml_str.as_bytes())?;
        Ok(())
    }

    pub fn generate_rfc_number(&mut self) -> u64 {
        self.rfc_counter += 1;
        self.rfc_counter
    }
}
